FROM ubuntu:latest

WORKDIR /app

RUN apt-get update
RUN apt-get -y  upgrade
RUN apt-get install -y gcc ruby
RUN gem install ceedling
