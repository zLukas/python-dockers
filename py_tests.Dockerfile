FROM ubuntu:latest

WORKDIR /app

RUN apt-get  update
RUN apt-get -y upgrade
RUN apt-get -y install python3-pip
RUN pip3 install -U pytest
RUN pip3 install intelhex
